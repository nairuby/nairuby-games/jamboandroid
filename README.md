# JamboAndroid

Jambo Dunia (Hello World) application demonstrating use of GitLab CI to create an Android executable

## Compiling and downloading

The [C](http://www.open-std.org/jtc1/sc22/wg14/) file, 
[jambo_dunia/main.c](https://gitlab.com/nairuby/nairuby-games/jamboandroid/-/blob/master/jambo_dunia/main.c) 
is automatically compiled to generate an executable for Android 4 (API 14) by the 
GitLab continuous integration pipeline upon push of a commit. Check for a green 
tick, circled in the image below, and then click on the download executable icon, also 
circled.

![Image showing how to download executable by clicking on the download executable icon in the top left of the GitLab web interface](./GitLabDownload.png "Download")

If you make a fork of the repository, the pipeline will still run once you 
make a commit, but the badge will not appear. You should be able to download an
executable by using this address https://gitlab.com/%{project_path}/-/jobs/artifacts/master/raw/jambo_dunia/libs/armeabi-v7a/main.out?job=createexecutable
to make your own badge by going to settings and then general as indicated
[here](https://blog.martisak.se/2020/05/11/gitlab-ci-latex-pipeline/ "Isaksson, Martin 'How to annoy your co-authors: a Gitlab CI pipeline for LaTeX'")

## Installing

### You do not have Root permissions

Download the executable main.out to a computer (these instructions assume the 
computer runs Linux, but should be modifiable to work on other operating systems).  
Connect your Android device to your computer, for example using a USB cable. Go 
to the developer options on your Android device and enable USB debugging. On your 
computer install the
[Android command line tools](https://developer.android.com/studio#command-tools). 
Open up a terminal and go the to the location where you have downloaded the
executable main.out and then use abd to transfer the file
```bash
sudo adb start-server
adb push main.out /data/local/tmp/main.out
```
If `adb` is not on the path of your sudo user, you may need to put the full
path to the adb command for it to work. Then log in to your Android device 
and run the program
```bash
adb shell
cd /data/local/tmp
./main.out
```

If you wish, you can delete the executable, then exit the shell,
and finally stop the adb server
```bash
rm main.out
exit
sudo adb kill-server
```

You can also run the program on your Android device using a terminal 
program such as 
[Terminal Emulator](https://f-droid.org/en/packages/com.termoneplus/).

In this case, after transferring the executable to your Android device 
using adb push, do not delete the executable, but you can exit from adb 
shell and stop the adb server. To run the program within your terminal 
program on your Android device, go to the location where you pushed it
and then run it.
```bash
cd /data/local/tmp
./main.out
```

### You have Root permissions

Once you have downloaded the executable main.out, if it is not directly 
downloaded on your Android device, transfer it to your Android device, 
for example using 
[Primitive ftpd](https://f-droid.org/en/packages/org.primftpd/). 
Then start a terminal program, such as 
[Terminal Emulator](https://f-droid.org/en/packages/com.termoneplus/). 
Find the location of the main.out file and change it to an executable 
using your terminal emulator
```bash
chmod +755 main.out
```
Then run the program
```bash
./main.out
```

### Further Notes

It is possible to use ADB on an Android device to transfer files to another 
Android device.  See: 
- Remote ADB Shell on [on GitHub](https://github.com/cgutman/RemoteAdbShell) 
and [on Google Play](https://play.google.com/store/apps/details?id=com.cgutman.androidremotedebugger&hl=en&gl=US)
- [AdbLib](https://github.com/cgutman/AdbLib)

## References

 - [Building an Android Command-Line Application Using the NDK Build Tools](https://software.intel.com/content/www/us/en/develop/articles/building-an-android-command-line-application-using-the-ndk-build-tools.html)
 - [Setting up an Android NDK development environment in Ubuntu Linux](https://subscription.packtpub.com/book/application_development/9781849691505/1/ch01lvl1sec10/setting-up-an-android-ndk-development-environment-in-ubuntu-linux)
 - [Install Android SDK tools in linux. Can be used for custom CI (Continuous Integration)](https://gist.github.com/Ashok-Varma/6b5864c1e444a3f1b61158254a43c4bc) 
- [Writeable and Executable location on Android](https://stackoverflow.com/questions/13690419/writeable-and-executable-location-on-android)
- [ADB Android Device Unauthorized](https://stackoverflow.com/questions/23081263/adb-android-device-unauthorized/25546300#25546300)
- [Connect Mobile Device with Android Debug Bridge (ADB) to USB,WiFi](https://www.guru99.com/adb-connect.html)
